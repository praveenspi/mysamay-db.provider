
/**
 * Abstract DB Provider for connections.
 */
export abstract class DBProvider {
    
    client: any;
    config: any;

    /**
     * Load config from local config file.
     */
    abstract loadConfig(): void;

    /**
     * Load config using parameter
     * @param config 
     */
    abstract loadConfigRemote(config: any);

    /**
     * Create connection string and connect to DB.
     */
    abstract connect(): void;

    /**
     * Disconnect from DB
     */
    abstract disconnect(): void;

    /**
     * Returns the customer DB by dbName for operations.
     * 
     * Also creates new connection if not connected.
     * @param config 
     */
    abstract getDB(config: any | null): Promise<any>;
}
