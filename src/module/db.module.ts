import { Module } from '@nestjs/common';
import { MongoProvider } from '../providers/mongodb/mongo-provider';
import { SqlProvider } from '../providers/mysql/sql-provider';

@Module({
    imports: [],
    providers: [
      MongoProvider,
      SqlProvider
    ],
    exports: [
        MongoProvider,
        SqlProvider
    ]
  })
export class DbModule {}
