import { Db } from 'mongodb';
import { Test, TestingModule } from '@nestjs/testing';
import { MongoProvider } from './mongo-provider';
import fs from 'fs';

describe('MongoProvider', () => {
  let provider: MongoProvider;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [MongoProvider],
    }).compile();

    provider = module.get<MongoProvider>(MongoProvider);
  });

  it('should be defined', () => {
    expect(provider).toBeDefined();
  });

  it('should load config from local file', () => {
    let mongoConfig = JSON.parse(fs.readFileSync("./properties/conf/mongoConfig.json", { encoding: "utf-8" }));
    expect(provider.loadConfig()).toEqual(mongoConfig);
  });

  it('should load config from remote', () => {
    let mongoConfig = JSON.parse(fs.readFileSync("./properties/conf/mongoConfig.json", { encoding: "utf-8" }));
    expect(provider.loadConfigRemote(mongoConfig)).toEqual(mongoConfig);
  });

  it('should get DB', async () => {
    expect(await provider.getDB(null))
      .toBeInstanceOf(Db)

    await provider.disconnect();
  });

  it('should return connected', async () => {
    let db = await provider.getDB(null);
    expect(await provider.getDB(null))
      .toEqual(db);

    await provider.disconnect();
  });

  it('should throw error', async () => {
    const mockExit = jest.spyOn(process, 'exit').mockImplementation((code?: number) => {throw("err")});
    try {
      await provider.getDB({mongoHosts: "1234"});
    }
    catch(error) {
      expect(error).toBeDefined();
    }
  });

});
