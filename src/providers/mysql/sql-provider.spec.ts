import { Test, TestingModule } from '@nestjs/testing';
import { SqlProvider } from './sql-provider';

describe('SqlProvider', () => {
  let provider: SqlProvider;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [SqlProvider],
    }).compile();

    provider = module.get<SqlProvider>(SqlProvider);
  });

  it('should be defined', () => {
    expect(provider).toBeDefined();
  });
});
